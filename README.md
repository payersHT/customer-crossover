# Customer Crossover Analysis


# Table of contents
1. Overview
2. R Scripts
3. SQL Scripts
4. Customer Crossover Dollar Amount


## Overview
This analysis will count the number of unique individual ids for a given UDA. It will also count the number of individual ids that have bought two UDAs. Here is an example looking at the 2 UDA MYHERO and NARUTO. 

|   | MYHERO | NARUTO|
|---|--------|-------|
|MYHERO| 7800313 |208768|
|NARUTO|208768 | 509805|

In position (1,1) (row one, column one), we see that 780,313 individuals bought at least one product with the MYHERO DUA attached to it. If we want to see how many individuals purchased both MYHERO and NARUTO, we will look at (1,2) or (2,1), which tells us 208,768 individuals bought products that had the UDA MYHERO and NARUTO.


## R Scripts
This directory contains the R script that will run the analysis. Three lines of code need to be adjusted. First is the number of UDAs you would like to run the analysis on. The second will be the file output name. This argument needs to be a string. Lastly will be the CSV that has the raw data. The SQL script for building this file can be found in SQL_scripts.

## SQL Scripts
This directory has the primary query used to pull the raw data. Please make sure to save the data as a CSV file.

## Customer Crossover Dollar Amount
This analysis is a work in progress. Still in early dev work. I will be adding the ability to calculate how much money a UDA group is spending.
