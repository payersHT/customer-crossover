SELECT individual_id, 
        b.uda_value_description,
        a.TRANSACTION_NUMBER, 
        a.QUANTITY,
        a.DOLLAR_VALUE_US
FROM dm_owner.transaction_detail_mv a
INNER JOIN dm_owner.product_uda_mv b
    on a.item_number = b.item
where a.brand_org_code = 'HT'
and TRUNC(a.txn_date) BETWEEN  '01-OCT-21' AND '31-MAR-22'
and uda_id in ('28');