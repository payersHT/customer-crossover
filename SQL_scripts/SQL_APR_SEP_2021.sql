SELECT individual_id, 
        b.uda_value_description,
        a.TRANSACTION_NUMBER, 
        a.QUANTITY,
        a.DOLLAR_VALUE_US
FROM dm_owner.transaction_detail_mv a
INNER JOIN dm_owner.product_uda_mv b
    on a.item_number = b.item
where a.brand_org_code = 'HT'
and TRUNC(a.txn_date) BETWEEN  '01-APR-21' AND '30-SEP-21'
and uda_id in ('28');